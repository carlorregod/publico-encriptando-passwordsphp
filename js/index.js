function operacion()
{
    //Capturando los radiobox. Entregan el valor asignado
    var password=$("#password").val();
    //Capturando el token generado
    var token=$("#token").val();

    //Envío por AJAX por post...
    $.ajax({
    type: "POST",
    url: "php/encriptacion.php",
    data: {'password': password,
            'token': token,
            'boton': 1}
    })
    .done(function(pwencriptada){
        alert('Password '+password+' encriptada es: '+pwencriptada);
    })
    .fail(function(){
        alert('Error!')
    })
}

function operacion2()
{
    //Capturando los radiobox. Entregan el valor asignado
    var password=$("#password").val();
    //Envío por AJAX por post...
    $.ajax({
    type: "POST",
    url: "php/encriptacion.php",
    data: {'password': password,
            'boton':2}
    })
    .done(function(pwencriptada){
        alert('Password comprobado');
    })
    .fail(function(){
        alert('Error 2!')
    })
}