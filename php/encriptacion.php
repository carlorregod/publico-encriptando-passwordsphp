<?php

function SetEncriptando()
{
    $pass = $_POST['password'];  
    /*Encriptado forma larga:  
    $salt = md5($pass);
    $pasword_encriptado = crypt($pass, $salt);
    */
    /*FORMA MÁS FÁCIL PARA ENCRIPTAR Y LA MÁS "SEGURA" */
    $password_encriptado = password_hash($pass, PASSWORD_BCRYPT);
    /*Esto es para el tema del token*/
    $token = crypt($_POST['token']);
    $_SESSION['token'] = $token;

    //Retorno del pw encriptado
    return $password_encriptado." ".$token;
};

function compruebaPassword($pasword_encriptado)
{
    $pass = $_POST['password'];
    return password_verify($pass, $pasword_encriptado);
};

function chequeaToken()
{
    /*PREVIAMENTE HAY QUE CONECTARSE A LA BD

    $result = $stm->fetch(PDO::FETCH_ASSOC);Este método se debe adaptar a como seala query desde la BD para obtener la variable token
    $query_token = "SELECT token FROM usuario WHERE id=$usuario";
    $result = pg_query($pgsql, $query_token) or die('Falló la query: ' . pg_last_error());
    $token = pg_fetch_object($result)->token;
    //$token= $result['token'];
    if($_SESSION['token'] != $token)
    {
        header("Location: index.php");
        exit();
    }
    */
}

$boton=$_POST['boton'];
if($boton == 1)
    echo SetEncriptando();
else if($boton ==2)
    echo compruebaPassword(SetEncriptando());